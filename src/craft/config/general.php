<?php

/**
* General Configuration
*
* All of your system's general configuration settings go in here.
* You can see a list of the default settings in craft/app/etc/config/defaults/general.php
*/

return array(
    '*' => array(
        'omitScriptNameInUrls' => true,
        'overridePhpSessionLocation' => true,
        'backupDbOnUpdate' => false,
        'allowAutoUpdates' => false,
        'cache' => true,
        'siteUrl' => 'https://template.com'
    ),

    '{{ cli_domain }}' => array(
        'omitScriptNameInUrls' => true,
        'allowAutoUpdates' => false,
        'siteUrl' => 'https://template.com'
    ),

    'staging.{{ cli_domain }}' => array(
        'omitScriptNameInUrls' => true,
        'allowAutoUpdates' => false,
        'siteUrl' => 'https://staging.template.com'
    ),

    '*.docker' => array(
        'omitScriptNameInUrls' => true,
        'devMode' => true,
        'cache' => false,
        'siteUrl' => 'http://*.docker'
    )
);
