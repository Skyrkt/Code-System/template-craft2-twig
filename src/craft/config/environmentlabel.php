<?php

return array(

	'*' => array(
		'label' => getenv('CRAFT_ENV_LABEL'),
		'suffix' => " // NO LIVE CONTENT",
	),

	'{{ cli_domain }}' => array(
		'showLabel' => false,
	),

	'*.dev' => array(
		'labelColor' => '#337799',
	)

);
